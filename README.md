# cicd/charts

## development

run SDK console:

```sh
bash tools.sh console
```

## testing

in console:

```sh
# all charts
tools.sh unittest

# single chart
cd /workspace/charts/workload
helm unittest .
```
