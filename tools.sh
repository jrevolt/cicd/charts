#!/usr/bin/env bash

compose() {
  local f efiles="" cfiles=""

  : "${ENV:=default}"
  f=compose.env && test -f "$f" && efiles+="$f,"
  f=compose.local.env && test -f "$f" && efiles+="$f,"
  f=compose.$ENV.env && test -f "$f" && efiles+="$f,"
  f=compose.$ENV.local.env && test -f "$f" && efiles+="$f,"

  f=compose.yml && test -f "$f" && cfiles+="$f:"
  f=compose.local.yml && test -f "$f" && cfiles+="$f:"
  f=compose.$ENV.yml && test -f "$f" && cfiles+="$f:"
  f=compose.$ENV.local.yml && test -f "$f" && cfiles+="$f:"

  ENV=$ENV \
  COMPOSE_ENV_FILES=${efiles%,} \
  COMPOSE_FILE=${cfiles%:} \
  COMPOSE_PATH_SEPARATOR=":" \
  docker compose "$@"
}

build() {
  compose build "$@"
}

run() {
  compose run --rm "$@"
}

sdk() {
  compose run --build sdk "$@"
}

console() {
  sdk "$@"
}

unittest() {
  helm unittest --parallel charts/*
}

"$@"
