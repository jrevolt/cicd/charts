FROM alpine as curl
ARG BUILDPLATFORM
ARG TARGETPLATFORM
RUN apk add curl tar
SHELL [ "/bin/sh", "-xc" ]

#https://kubernetes.io/releases/

FROM curl as download-kubectl127
RUN curl -LO https://dl.k8s.io/release/v1.27.1/bin/${TARGETPLATFORM}/kubectl

FROM curl as download-kubectl126
RUN curl -LO https://dl.k8s.io/release/v1.26.1/bin/${TARGETPLATFORM}/kubectl

FROM curl as download-kubectl125
RUN curl -LO https://dl.k8s.io/release/v1.25.6/bin/${TARGETPLATFORM}/kubectl

FROM curl as download-kubectl124
RUN curl -LO https://dl.k8s.io/release/v1.24.10/bin/${TARGETPLATFORM}/kubectl

FROM curl as download-kubectl123
RUN curl -LO https://dl.k8s.io/release/v1.23.16/bin/${TARGETPLATFORM}/kubectl

FROM curl as download-kubectl122
RUN curl -LO https://dl.k8s.io/release/v1.22.17/bin/${TARGETPLATFORM}/kubectl

FROM curl as download-kubectl121
RUN curl -LO https://dl.k8s.io/release/v1.21.14/bin/${TARGETPLATFORM}/kubectl

FROM curl as download-kubectl120
RUN curl -LO https://dl.k8s.io/release/v1.20.15/bin/${TARGETPLATFORM}/kubectl

FROM curl as download-helm
# HELM_VERSION has to be the same as in the pipeline helm.variables.helm_image.
ARG HELM_VERSION="3.15.1"
RUN PLATFORM=${TARGETPLATFORM//\//-} &&\
    curl -sL https://get.helm.sh/helm-v${HELM_VERSION}-${PLATFORM}.tar.gz | tar xzv -C / --strip-components=1 ${PLATFORM}/helm &&\
    ls -l /helm
RUN /helm version

FROM curl as download-k9s
ARG K9S_VERSION="0.27.0"
RUN PLATFORM=${TARGETPLATFORM//linux\//} &&\
    curl -sL https://github.com/derailed/k9s/releases/download/v${K9S_VERSION}/k9s_Linux_${PLATFORM}.tar.gz | tar xzv -C / k9s
RUN /k9s version

FROM curl as download-oci-cli
ARG OCI_CLI_VERSION=3.34.0
RUN cd /tmp &&\
    url=https://github.com/oracle/oci-cli/releases/download/v${OCI_CLI_VERSION}/oci-cli-${OCI_CLI_VERSION}.zip &&\
    (curl -LO $url || curl -LO $url) &&\
    cd / && unzip /tmp/*.zip oci-cli/oci_cli-${OCI_CLI_VERSION}-py3-none-any.whl

FROM curl as download-kubelogin
ARG KUBELOGIN_VERSION="0.0.32"
RUN PLATFORM=${TARGETPLATFORM//linux\//} &&\
    cd /tmp &&\
    wget https://github.com/Azure/kubelogin/releases/download/v${KUBELOGIN_VERSION}/kubelogin-linux-${PLATFORM}.zip &&\
    unzip *.zip &&\
    install bin/*/kubelogin /kubelogin -m750

FROM alpine:3.18.4 as base
ARG TARGETPLATFORM
RUN --mount=type=cache,target=/etc/apk/cache,id=apk-$TARGETPLATFORM \
    apk add bash curl vim htop coreutils moreutils git jq &&\
    apk add py3-pip && pip install yq &&\
    apk add ansible && ansible-galaxy collection install kubernetes.core:2.3.2 &&\
    apk add dotnet6-sdk dotnet6-runtime &&\
      dotnet tool install --tool-path=/opt/dotnet JRevolt.YamlSecrets.Cli --version="1.*" &&\
      apk del dotnet6-sdk
# oci-cli
# https://medium.com/@davidcesc/oracle-client-on-alpine-linux-177835823b34
# https://github.com/Shrinidhikulkarni7/OracleClient_Alpine/blob/master/Dockerfile
# https://github.com/oracle/oci-cli/releases
RUN --mount=type=cache,target=/etc/apk/cache,id=apk-$TARGETPLATFORM \
    --mount=from=download-oci-cli,src=/oci-cli,dst=/mnt/oci-cli \
    apk add libaio libnsl libc6-compat &&\
    pip3 install /mnt/oci-cli/oci_cli-*.whl
# azure-cli
# https://learn.microsoft.com/en-us/cli/azure/install-azure-cli
# https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=script
RUN --mount=type=cache,target=/etc/apk/cache,id=apk-$TARGETPLATFORM \
    --mount=type=cache,target=/root/.cache,id=root-cache-$TARGETPLATFORM \
    builddeps="gcc musl-dev python3-dev libffi-dev openssl-dev cargo make" &&\
    apk add $builddeps && pip install azure-cli && apk del $builddeps
    # ^^ not sure about this cleanup but it seems safe to remove these build-time tools/libs
ENV HELM_PLUGINS="/opt/helm/plugins"
RUN --mount=from=download-k9s,source=/k9s,target=/k9s \
    --mount=from=download-kubectl127,source=/kubectl,target=/mnt/kubectl127 \
    --mount=from=download-kubectl126,source=/kubectl,target=/mnt/kubectl126 \
    --mount=from=download-kubectl125,source=/kubectl,target=/mnt/kubectl125 \
    --mount=from=download-kubectl124,source=/kubectl,target=/mnt/kubectl124 \
    --mount=from=download-kubectl123,source=/kubectl,target=/mnt/kubectl123 \
    --mount=from=download-kubectl122,source=/kubectl,target=/mnt/kubectl122 \
    --mount=from=download-kubectl121,source=/kubectl,target=/mnt/kubectl121 \
    --mount=from=download-kubectl120,source=/kubectl,target=/mnt/kubectl120 \
    --mount=from=download-kubelogin,source=/kubelogin,target=/mnt/kubelogin \
    --mount=from=download-helm,source=/helm,target=/mnt/helm \
    install /k9s /usr/local/bin/k9s -m555 &&\
    install /mnt/kube* /usr/local/bin/ -m555 &&\
    ln -s /usr/local/bin/kubectl122 /usr/local/bin/kubectl &&\
    install /mnt/helm /usr/local/bin/helm -m555 &&\
    helm plugin install https://github.com/databus23/helm-diff --version=v3.6.0 >/dev/null &&\
    helm plugin install https://gitlab.com/jrevolt/cicd/yamlsecrets-helm-plugin.git --version=1.0.0
RUN --mount=type=cache,target=/var/cache/apk,id=apk-$TARGETPLATFORM \
    apk add rsync findutils bash-completion
ENV PATH="/opt/dotnet:$PATH"
RUN --mount=target=/context \
    install /context/bin/*.sh /usr/local/bin/ -m555 &&\
    install -m600 \
      /context/bin/.bash_profile \
      /context/bin/.bashrc \
      /context/bin/.bash_completion \
      /root/
WORKDIR /deployment
ENTRYPOINT [ "entrypoint.sh" ]
