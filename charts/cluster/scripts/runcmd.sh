#!/bin/bash

# extract config from guestinfo
vmtoolsd --cmd 'info-get guestinfo.ovfenv' > /tmp/ovfenv
extract() { sed -nre 's/.*guestinfo.'$1'.*value="([^"]+)".*/\1/p' /tmp/ovfenv; }
ip=$(extract ip)
gw=$(extract gateway)
nm=$(extract netmask)
ns=$(extract dns)
ipsuffix=${ip//*./}   # get last component in IPv4
pnum=${ipsuffix:0:1}  # 1st number is a pool number
nnum=${ipsuffix:1:2}  # remainder is a VM index
hostname=$(hostname | sed -re 's/([^-]+)-([^-]+)-.*/\1'$pnum'\2'$nnum'/') # e.g. x.x.x.123 => pnum=1,nnum=23 => cluster1pool23
tee $(ls -1 /etc/netplan/*.yaml | head -n1) <<EOF
network:
  version: 2
  renderer: networkd
  ethernets:
    ens192:
      addresses: [ $ip/24 ]
      routes:
      - to: default
        via: $gw
      nameservers:
        addresses: [ $ns ]
EOF

# apply network config
hostnamectl set-hostname $hostname
netplan apply

#https://askubuntu.com/questions/719058/blk-update-request-i-o-error-dev-fd0-sector-0
rmmod floppy
echo "blacklist floppy" | tee /etc/modprobe.d/blacklist-floppy.conf
dpkg-reconfigure initramfs-tools

#https://longhorn.io/kb/troubleshooting-volume-with-multipath/
#https://github.com/longhorn/longhorn/issues/6755
#https://github.com/kubernetes/kubernetes/issues/60894
systemctl stop multipathd
systemctl disable multipathd

#kubectl
curl -sL https://dl.k8s.io/release/v1.28.4/bin/linux/amd64/kubectl | install /dev/stdin /usr/local/bin/kubectl -m555

#k9s
curl -sL https://github.com/derailed/k9s/releases/download/v0.28.2/k9s_Linux_amd64.tar.gz | tar xzv -C /usr/local/bin k9s

#KUBECONFIG
tee -a /etc/environment <<EOF
KUBECONFIG="/etc/rancher/rke2/rke2.yaml"
EOF
