
# find folder specified by $path, creating any missing folders
function Get-FolderByPath($datacenter,$type,$path) {
    $dcroot = Get-Datacenter $datacenter
    $root = Get-Folder -Location $dcroot -Type $type -NoRecursion
    $currentFolder = $root
    foreach ($part in $path -split '/') {
        $found = Get-Folder -Location $currentFolder -Type $type -Name $part -NoRecursion -ErrorAction SilentlyContinue
        if ($null -eq $found) {
            $found = New-Folder -Location $currentFolder -Name $part
        }
        $currentFolder = $found
    }
    return $currentFolder
}

# find resource pool specified by $path
function Get-ResourcePoolByPath($hostsystem,$path) {
    $vmhost = Get-VMHost $hostsystem
    $root = Get-ResourcePool -Location $vmhost -NoRecursion
    $current = $root
    foreach ($part in $path -split '/') {
        $current = Get-ResourcePool -Location $current -Name $part -NoRecursion
    }
    return $current
}

function ProcessTemplates($cfg) {
    foreach ($clname in $cfg.clusters.Keys) {

        $clcfg = $cfg.clusters[$clname]

        $folder = Get-FolderByPath -datacenter $cfg.vsphere.datacenter -type VM -path $cfg.vsphere.folder
        $clfolder = Get-FolderByPath -datacenter $cfg.vsphere.datacenter -type VM -path "$($cfg.vsphere.folder)/${clname}" -create:$true

        $rpool = Get-ResourcePoolByPath -hostsystem $cfg.vsphere.hostsystem -path $cfg.vsphere.folder

        foreach ($poolName in $clcfg.nodePools.Keys) {

            $vmname = "${clname}-${poolName}"

            Write-Host "[$vmname] Configuring template"
            $vmcfg = $clcfg.nodePools[$poolName]

            $template = Get-Template -Name $clcfg.template -Location $folder

            $vm = Get-VM -Name $vmname -ErrorAction SilentlyContinue
            if ($null -eq $vm) {
                Write-Host "[$vmname] Converting existing VM template to a VM for editing..."
                $vm = Get-Template -Name $vmname -ErrorAction SilentlyContinue | Set-Template -ToVM
            }
            if ($null -eq $vm) {
                Write-Host "[$vmname] Cloning from $($template.Name)"
                $vm = New-VM -Name $vmname -Template $template -Datastore $cfg.vsphere.datastore -VMHost $cfg.vsphere.hostsystem -Location $clfolder -ResourcePool $rpool
            }

            $vmView = Get-View $vm
            $vmExtensionData = $vm | Get-View
            $vAppConfig = $vmExtensionData.Config.vAppConfig

            # vApp config
            # enable static IP pool
            $vAppConfigSpec = New-Object VMware.Vim.VmConfigSpec
            $ipAlloc = New-Object VMware.Vim.VAppIPAssignmentInfo
            $ipAlloc.ipAllocationPolicy = [VMware.Vim.VAppIPAssignmentInfoIpAllocationPolicy]::fixedAllocatedPolicy
            $ipAlloc.supportedAllocationScheme = "ovfenv"
            $ipAlloc.ipProtocol = "IPv4"
            $vAppConfigSpec.ipAssignment = $ipAlloc
            $vAppConfigSpec.OvfEnvironmentTransport = @("com.vmware.guestInfo")
            $vmConfigSpec = New-Object VMware.Vim.VirtualMachineConfigSpec
            $vmConfigSpec.vAppConfig = $vAppConfigSpec
            $_ = $vm.ExtensionData.ReconfigVM_Task($vmConfigSpec)

            # processing disks
            # normalize input, $disks must be an array
            $disks = Get-HardDisk -VM $vm
            if ($null -eq $disks) { $disks = @() }
            if ($disks -isnot [array]) { $disks = ,$disks }

            # add/resize disks
            Write-Host "[$vmname] Configuring $($vmcfg.disks.Count) disks"
            for ($i=0; $i -lt $vmcfg.disks.Count; $i++) {
                $size = $vmcfg.disks[$i]
                if ($i -gt $disks.Length - 1) {
                    # no such disk, create new
                    Write-Host "[$vmname] Adding ${size} GB disk"
                    $disks += New-HardDisk -VM $vm -CapacityGB $size -StorageFormat Thin
                } else {
                    # existing disk, resize if needed
                    $disk = $disks[$i]
                    if ($disk.CapacityGB -lt $size) {                        
                        Write-Host "[$vmname] Resizing disk $($disk.Name), size ${size}"
                        $_ = Set-HardDisk -HardDisk $disk -CapacityGB $size -Confirm:$false
                    } else {
                        # just report findings
                        Write-Host "[$vmname] Found disk $($disk.Name), size $($disk.CapacityGB) GB (requested ${size})"
                    }
                }
            }
            #Get-HardDisk -VM $vm | Select-Object Name,CapacityGB

            Write-Host "[$vmname] Converting to VM template"
            $vm = Set-VM -VM $vm -ToTemplate -Confirm:$false
        }
    }
}

function Main() {
    $basedir = $PSScriptRoot

    $ErrorActionPreference = 'Stop'

    # load yaml configuration
    $cfg = Get-Content -Path "$basedir/vmdeploy.local.yaml" -Raw | ConvertFrom-Yaml

    # connect to vCenter
    $con = Connect-VIServer -Server $cfg.vcenter.host -Port $cfg.vcenter.port -Protocol https -User $cfg.vcenter.user -Password $cfg.vcenter.pass -Force
    Write-Host "Connected to $con"

    ProcessTemplates($cfg)

    Disconnect-VIServer -server $cfg.vcenter.host -Confirm:$false
    Write-Host "Done"
}

Main
