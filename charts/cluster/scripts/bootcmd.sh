#!/bin/bash
fsresize() {
  local disk part
  if [[ "$1" =~ ([^0-9]+)([0-9]+)$ ]]; then
    disk=${BASH_REMATCH[1]}
    part=${BASH_REMATCH[2]}
  fi

  # validate
  [[ -b "${disk}${part}" ]] || return 1

  # resize partition and file system
  growpart "$disk" "$part"
  resize2fs "${disk}${part}"

  # report
  df -h "${disk}${part}"
}

fsresize_all() {
  for i in $(ls /dev/sd[b-z]1 2>/dev/null); do
    fsresize $i
  done
}

fsresize_all
