<#
.PARAMETER config
    path to vsphere.yaml config
#>
param (
    [Parameter(Mandatory=$false)]
    [string]$config = "$basedir/vsphere.local.yaml"
)

# can't use $PSScriptRoot in param() above, this is workaround
$basedir=$PSScriptRoot


# this should be provided by docker image used to run this script
# here only for local development and docs purposes
function Prerequisites() {
    Install-Module -Name powershell-yaml
    Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -Confirm:$false | Out-Null
}


# main entrypoint
# connects to vCenter and starts provisioning vSphere configuration for each defined cluster
function Main($YamlFile = $config) {

    $ErrorActionPreference = 'Stop'

    # load yaml configuration
    $cfg = Get-Content -Path $YamlFile -Raw | ConvertFrom-Yaml
    # connect to vCenter
    $con = Connect-VIServer -Server $cfg.vcenter.host -Port $cfg.vcenter.port -Protocol https -User $cfg.vcenter.user -Password $cfg.vcenter.pass -Force
    Write-Host "Connected to $con"
    foreach ($clname in $cfg.clusters.Keys) {
        # configure cluster
        MakeCluster $clname $cfg.clusters[$clname]
    }
}

# configures cluster objects: VM folder, network folder, resource pool, and cluster network
function MakeCluster($clname, $cldata) {
    Write-Host "[$clname] MakeCluster()"
    $_ = MakeVMFolder $clname $cldata
    $netFolder = MakeNetworkFolder $clname $cldata
    $_ = MakeResourcePool $clname $cldata
    $_ = MakeClusterNetwork $clname $cldata $netFolder
}

# create VM folder
function MakeVMFolder($clname, $cldata) {
    $folders = $cldata.vsphere.folder -replace "%cluster%",$clname -replace "{{.cluster}}",$clname -split "/"
    Write-Host "[$clname] MakeVMFolder($($folders -join "/"))"
    $vcDatacenter = Get-Datacenter $cfg.vsphere.datacenter
    $fparent = Get-Folder -Location $vcDatacenter -Name "vm"
    foreach ($fname in $folders) {
        $f = Get-Folder -Location $fparent -Name $fname -ErrorAction SilentlyContinue
        if ($null -eq $f) {
            $f = New-Folder -Location $fparent -Name $fname
        }
        $fparent = $f
    }
    return $fparent
}

# create network folder
function MakeNetworkFolder($clname, $cldata) {
    $folders = $cldata.vsphere.folder -replace "%cluster%|{{.cluster}}",$clname -split "/"
    Write-Host "[$clname] MakeNetworkFolder($($folders -join "/"))"
    $vcDatacenter = Get-Datacenter $cldata.vsphere.datacenter
    $fparent = Get-Folder -Location $dc -Name "network"
    foreach ($fname in $folders) {
        $f = Get-Folder -Location $fparent -Name $fname -ErrorAction SilentlyContinue
        if ($null -eq $f) {
            $f = New-Folder -Location $fparent -Name $fname
        }
        $fparent = $f
    }
    return $fparent
}

# create resource pool with default settings
# configuration of any specific resource pool options is out of scope
function MakeResourcePool($clname,$cldata) {
    $folders = $cldata.vsphere.pool -replace "%cluster%|{{.cluster}}",$clname -split "/"
    Write-Host "[$clname] MakeResourcePool($($folders -join "/"))"
    $vcDatacenter = Get-Datacenter $cldata.vsphere.datacenter
    $vcHost = Get-VMHost $cldata.vsphere.hostsystem

    $parent = $vcHost
    foreach ($name in $folders) {
        $rp = Get-ResourcePool -Location $parent -Name $name -ErrorAction SilentlyContinue
        if ($null -eq $rp) {
            $rp = New-ResourcePool -Location $parent -Name $name
        }
        $parent = $rp
    }
    return $parent
}

# create networks for all node pools defined by a cluster
function MakeClusterNetwork($clname,$cldata,$folder) {
    foreach ($pname in $cldata.pools.Keys) {
        Write-Host "[$clname,$pname] MakeClusterNetwork()"
        $pdata = $cldata.pools[$pname]
        MakeNetworkProfile $clname $cldata $pname $pdata $folder
    }
}


# create network protocol profile for a given node pool of a cluster
function MakeNetworkProfile($clname,$cldata,$pname,$pdata,$folder) {
    $network = "$clname-$pname"
    $subnet = [string]$cldata.subnet
    $prefix = $subnet -replace "\.0$",""

    # ipRange is from..to, e.g 11..19, which translates to vSphere IPPool range x.x.x.11#9
    $ipItems = $pdata.ipRange -split "\.\."
    $ipFrom = $ipItems[0]
    $ipSize = $ipItems[1] - $ipFrom + 1
    $iprange = "${prefix}.${ipFrom}#${ipSize}"

    Write-Host "[$clname,$pname] MakeNetworkProfile($network) iprange=$iprange"
    $vdp = Get-VirtualPortGroup -Name $network -ErrorAction SilentlyContinue
    if ( $null -eq $vdp ) {
        $sw = Get-VirtualSwitch -Name $pdata.vsphere.switch -VMHost $pdata.vsphere.hostsystem
        $vdp = New-VirtualPortGroup -Name $network -VirtualSwitch $sw -VLanId 0
        # we may need to wait a bit until it's ready
        $moref = $null
        while ($null -eq $moref) {
            $found = Get-View -ViewType Network -Filter @{"Name" = $network} -ErrorAction SilentlyContinue
            if ($null -eq $found) {
                Write-Host "[$clname,$pname] Get-View $network not ready..."
                Start-Sleep -Seconds 5
            } else {
                $moref = $found.moref
            }
        }
        # this ugly trick moves network into its network folder
        (Get-View $folder).MoveIntoFolder([VMware.Vim.ManagedObjectReference[]]@($moref))
    }

    # setup IP pool
    $p = New-Object VMware.Vim.IpPool
    $p.name = $network
    $p.ipv4Config = New-Object VMware.Vim.IpPoolIpPoolConfigInfo
    $p.ipv4Config.subnetAddress = $cldata.subnet
    $p.ipv4Config.netmask = $cldata.netmask
    $p.ipv4Config.gateway = $cldata.gateway
    $p.ipv4Config.range = $iprange
    $p.ipv4Config.dns = New-Object System.String[] (1)
    $p.ipv4Config.dns[0] = $cldata.dns
    $p.ipv4Config.dhcpServerAvailable = $false
    $p.ipv4Config.ipPoolEnabled = $true

    $net = Get-View -ViewType Network -Filter @{"Name"="^$network$"}
    $p.networkAssociation = New-Object VMware.Vim.IpPoolAssociation[] (1)
    $p.networkAssociation[0] = New-Object VMware.Vim.IpPoolAssociation
    $p.networkAssociation[0].network = $net.MoRef
    $p.networkAssociation[0].networkName = $network

    $dc = Get-Datacenter $cldata.vsphere.datacenter
    $PoolManager = Get-View -Id 'IpPoolManager-IpPoolManager'

    Write-Host "[$clname,$pname] Configuring IP pool $network ($iprange)"
    $x = $PoolManager.QueryIpPools($dc.Id) | Where-Object { $_.Name -eq $p.Name }
    if ($null -eq $x) {
        Write-Host "[$clname,$pname] CreateIpPool($network)"
        $PoolManager.CreateIpPool($dc.ExtensionData.MoRef, $p) | Out-Null
        $x = $PoolManager.QueryIpPools($dc.Id) | Where-Object { $_.Name -eq $p.Name }
    } else {
        Write-Host "[$clname,$pname] UpdateIpPool($($x.Id))"
        $p.Id = $x.Id
        $PoolManager.UpdateIpPool($dc.ExtensionData.MoRef, $p) | Out-Null
    }

    return
}

Main
