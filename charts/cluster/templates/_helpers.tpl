{{- define "cluster.configure.annotations"}}
{{- $anns := merge dict .Values.configure.annotations}}
{{- if hasKey $anns "helm.sh/hook"}}
{{- $_ := set $anns "helm.sh/hook-weight" (.weight | default "0")}}
{{- end}}
{{- $anns | toYaml | trimPrefix "{}"}}
{{- end}}

{{- define "cluster.configure.annotations.block"}}
{{- $anns := include "cluster.configure.annotations" $}}
{{- if $anns}}
annotations:
  {{- $anns | nindent 2}}
{{- end}}
{{- end}}

{{/*
Returns runcmd script.
Designed as template function to allow override and laoding scripts from caller-given source
extra args sent by caller:
- poolName - node pool name, allows you to return pool-specific script
*/}}
{{- define "cluster.runcmd"}}
{{- .Files.Get "scripts/runcmd.sh"}}
{{- end}}

{{/*
Returns bootcmd script.
Designed as template function to allow override and laoding scripts from caller-given source
extra args sent by caller:
- poolName - node pool name, allows you to return pool-specific script
*/}}
{{- define "cluster.bootcmd"}}
{{- .Files.Get "scripts/bootcmd.sh"}}
{{- end}}

{{- define "cluster.findCloudCredentialByType"}}
{{- $credentials := .Values.cloudCredentials}}
{{- $type := .type | required "type"}}
{{- $result := ""}}
{{- $items := $credentials.items}}{{$items = omit $items "$name"}}
{{- range $k,$v := $items}}
{{- if $v.type | eq $type}}
{{- $result = printf "%s:%s-%s" $credentials.namespace $.Release.Name $k}}
{{- end}}
{{- end}}
{{- print $result}}
{{- end}}

{{- define "cluster.cloudCredentials.vmwarevsphere"}}
{{- include "cluster.findCloudCredentialByType" (merge dict $ (dict "type" "vmwarevsphere"))}}
{{- end}}

{{- define "cluster.cloudCredentials.s3"}}
{{- include "cluster.findCloudCredentialByType" (merge dict $ (dict "type" "s3"))}}
{{- end}}

{{- /* print release-scoped name of the auth. secret for a given .registry */}}
{{- define "cluster.registryconfig-auth.name"}}
{{- printf "%s-registryconfig-auth-%s" (.Release.Name) (.registry | replace "." "-")}}
{{- end}}
