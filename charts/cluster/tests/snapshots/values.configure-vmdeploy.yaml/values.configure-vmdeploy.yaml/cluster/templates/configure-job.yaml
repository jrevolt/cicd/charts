
apiVersion: batch/v1
kind: Job
metadata:
  name: release-name-vsphere-configure  
  annotations:
    helm.sh/hook: pre-install,pre-upgrade
    helm.sh/hook-delete-policy: before-hook-creation
    helm.sh/hook-weight: "0"
spec:
  backoffLimit: 0
  template:
    spec:
      restartPolicy: Never
      containers:
      - name: prepare-node-templates
        image: registry.gitlab.com/jrevolt/cicd/charts/cluster/powercli
        command:
        - pwsh
        - -Command
        - /scripts/vmdeploy.ps1
        volumeMounts:
        - mountPath: /scripts/vmdeploy.ps1
          name: scripts
          subPath: vmdeploy.ps1
        - mountPath: /scripts/vmdeploy.local.yaml
          name: config
          subPath: vmdeploy.local.yaml
      - name: prepare-vsphere-objects
        image: registry.gitlab.com/jrevolt/cicd/charts/cluster/powercli
        command:
        - pwsh
        - -Command
        - /scripts/vsphere.ps1
        - /scripts/vsphere.yaml
        volumeMounts:
        - mountPath: /scripts/vsphere.ps1
          name: scripts
          subPath: vsphere.ps1
        - mountPath: /scripts/vsphere.yaml
          name: config
          subPath: vsphere.yaml
      volumes:
      - name: scripts
        configMap:
          name: release-name-vsphere-configure
      - name: config
        secret:
          secretName: release-name-vsphere-configure
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: release-name-vsphere-configure  
  annotations:
    helm.sh/hook: pre-install,pre-upgrade
    helm.sh/hook-delete-policy: before-hook-creation
    helm.sh/hook-weight: "-1"
data:
  vsphere.ps1: |
    <#
    .PARAMETER config
        path to vsphere.yaml config
    #>
    param (
        [Parameter(Mandatory=$false)]
        [string]$config = "$basedir/vsphere.local.yaml"
    )
    
    # can't use $PSScriptRoot in param() above, this is workaround
    $basedir=$PSScriptRoot
    
    
    # this should be provided by docker image used to run this script
    # here only for local development and docs purposes
    function Prerequisites() {
        Install-Module -Name powershell-yaml
        Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -Confirm:$false | Out-Null
    }
    
    
    # main entrypoint
    # connects to vCenter and starts provisioning vSphere configuration for each defined cluster
    function Main($YamlFile = $config) {
    
        $ErrorActionPreference = 'Stop'
    
        # load yaml configuration
        $cfg = Get-Content -Path $YamlFile -Raw | ConvertFrom-Yaml
        # connect to vCenter
        $con = Connect-VIServer -Server $cfg.vcenter.host -Port $cfg.vcenter.port -Protocol https -User $cfg.vcenter.user -Password $cfg.vcenter.pass -Force
        Write-Host "Connected to $con"
        foreach ($clname in $cfg.clusters.Keys) {
            # configure cluster
            MakeCluster $clname $cfg.clusters[$clname]
        }
    }
    
    # configures cluster objects: VM folder, network folder, resource pool, and cluster network
    function MakeCluster($clname, $cldata) {
        Write-Host "[$clname] MakeCluster()"
        $_ = MakeVMFolder $clname $cldata
        $netFolder = MakeNetworkFolder $clname $cldata
        $_ = MakeResourcePool $clname $cldata
        $_ = MakeClusterNetwork $clname $cldata $netFolder
    }
    
    # create VM folder
    function MakeVMFolder($clname, $cldata) {
        $folders = $cldata.vsphere.folder -replace "%cluster%",$clname -replace "{{.cluster}}",$clname -split "/"
        Write-Host "[$clname] MakeVMFolder($($folders -join "/"))"
        $vcDatacenter = Get-Datacenter $cfg.vsphere.datacenter
        $fparent = Get-Folder -Location $vcDatacenter -Name "vm"
        foreach ($fname in $folders) {
            $f = Get-Folder -Location $fparent -Name $fname -ErrorAction SilentlyContinue
            if ($null -eq $f) {
                $f = New-Folder -Location $fparent -Name $fname
            }
            $fparent = $f
        }
        return $fparent
    }
    
    # create network folder
    function MakeNetworkFolder($clname, $cldata) {
        $folders = $cldata.vsphere.folder -replace "%cluster%|{{.cluster}}",$clname -split "/"
        Write-Host "[$clname] MakeNetworkFolder($($folders -join "/"))"
        $vcDatacenter = Get-Datacenter $cldata.vsphere.datacenter
        $fparent = Get-Folder -Location $dc -Name "network"
        foreach ($fname in $folders) {
            $f = Get-Folder -Location $fparent -Name $fname -ErrorAction SilentlyContinue
            if ($null -eq $f) {
                $f = New-Folder -Location $fparent -Name $fname
            }
            $fparent = $f
        }
        return $fparent
    }
    
    # create resource pool with default settings
    # configuration of any specific resource pool options is out of scope
    function MakeResourcePool($clname,$cldata) {
        $folders = $cldata.vsphere.pool -replace "%cluster%|{{.cluster}}",$clname -split "/"
        Write-Host "[$clname] MakeResourcePool($($folders -join "/"))"
        $vcDatacenter = Get-Datacenter $cldata.vsphere.datacenter
        $vcHost = Get-VMHost $cldata.vsphere.hostsystem
    
        $parent = $vcHost
        foreach ($name in $folders) {
            $rp = Get-ResourcePool -Location $parent -Name $name -ErrorAction SilentlyContinue
            if ($null -eq $rp) {
                $rp = New-ResourcePool -Location $parent -Name $name
            }
            $parent = $rp
        }
        return $parent
    }
    
    # create networks for all node pools defined by a cluster
    function MakeClusterNetwork($clname,$cldata,$folder) {
        foreach ($pname in $cldata.pools.Keys) {
            Write-Host "[$clname,$pname] MakeClusterNetwork()"
            $pdata = $cldata.pools[$pname]
            MakeNetworkProfile $clname $cldata $pname $pdata $folder
        }
    }
    
    
    # create network protocol profile for a given node pool of a cluster
    function MakeNetworkProfile($clname,$cldata,$pname,$pdata,$folder) {
        $network = "$clname-$pname"
        $subnet = [string]$cldata.subnet
        $prefix = $subnet -replace "\.0$",""
    
        # ipRange is from..to, e.g 11..19, which translates to vSphere IPPool range x.x.x.11#9
        $ipItems = $pdata.ipRange -split "\.\."
        $ipFrom = $ipItems[0]
        $ipSize = $ipItems[1] - $ipFrom + 1
        $iprange = "${prefix}.${ipFrom}#${ipSize}"
    
        Write-Host "[$clname,$pname] MakeNetworkProfile($network) iprange=$iprange"
        $vdp = Get-VirtualPortGroup -Name $network -ErrorAction SilentlyContinue
        if ( $null -eq $vdp ) {
            $sw = Get-VirtualSwitch -Name $pdata.vsphere.switch -VMHost $pdata.vsphere.hostsystem
            $vdp = New-VirtualPortGroup -Name $network -VirtualSwitch $sw -VLanId 0
            # we may need to wait a bit until it's ready
            $moref = $null
            while ($null -eq $moref) {
                $found = Get-View -ViewType Network -Filter @{"Name" = $network} -ErrorAction SilentlyContinue
                if ($null -eq $found) {
                    Write-Host "[$clname,$pname] Get-View $network not ready..."
                    Start-Sleep -Seconds 5
                } else {
                    $moref = $found.moref
                }
            }
            # this ugly trick moves network into its network folder
            (Get-View $folder).MoveIntoFolder([VMware.Vim.ManagedObjectReference[]]@($moref))
        }
    
        # setup IP pool
        $p = New-Object VMware.Vim.IpPool
        $p.name = $network
        $p.ipv4Config = New-Object VMware.Vim.IpPoolIpPoolConfigInfo
        $p.ipv4Config.subnetAddress = $cldata.subnet
        $p.ipv4Config.netmask = $cldata.netmask
        $p.ipv4Config.gateway = $cldata.gateway
        $p.ipv4Config.range = $iprange
        $p.ipv4Config.dns = New-Object System.String[] (1)
        $p.ipv4Config.dns[0] = $cldata.dns
        $p.ipv4Config.dhcpServerAvailable = $false
        $p.ipv4Config.ipPoolEnabled = $true
    
        $net = Get-View -ViewType Network -Filter @{"Name"="^$network$"}
        $p.networkAssociation = New-Object VMware.Vim.IpPoolAssociation[] (1)
        $p.networkAssociation[0] = New-Object VMware.Vim.IpPoolAssociation
        $p.networkAssociation[0].network = $net.MoRef
        $p.networkAssociation[0].networkName = $network
    
        $dc = Get-Datacenter $cldata.vsphere.datacenter
        $PoolManager = Get-View -Id 'IpPoolManager-IpPoolManager'
    
        Write-Host "[$clname,$pname] Configuring IP pool $network ($iprange)"
        $x = $PoolManager.QueryIpPools($dc.Id) | Where-Object { $_.Name -eq $p.Name }
        if ($null -eq $x) {
            Write-Host "[$clname,$pname] CreateIpPool($network)"
            $PoolManager.CreateIpPool($dc.ExtensionData.MoRef, $p) | Out-Null
            $x = $PoolManager.QueryIpPools($dc.Id) | Where-Object { $_.Name -eq $p.Name }
        } else {
            Write-Host "[$clname,$pname] UpdateIpPool($($x.Id))"
            $p.Id = $x.Id
            $PoolManager.UpdateIpPool($dc.ExtensionData.MoRef, $p) | Out-Null
        }
    
        return
    }
    
    Main
    
  vmdeploy.ps1: |
    
    # find folder specified by $path, creating any missing folders
    function Get-FolderByPath($datacenter,$type,$path) {
        $dcroot = Get-Datacenter $datacenter
        $root = Get-Folder -Location $dcroot -Type $type -NoRecursion
        $currentFolder = $root
        foreach ($part in $path -split '/') {
            $found = Get-Folder -Location $currentFolder -Type $type -Name $part -NoRecursion -ErrorAction SilentlyContinue
            if ($null -eq $found) {
                $found = New-Folder -Location $currentFolder -Name $part
            }
            $currentFolder = $found
        }
        return $currentFolder
    }
    
    # find resource pool specified by $path
    function Get-ResourcePoolByPath($hostsystem,$path) {
        $vmhost = Get-VMHost $hostsystem
        $root = Get-ResourcePool -Location $vmhost -NoRecursion
        $current = $root
        foreach ($part in $path -split '/') {
            $current = Get-ResourcePool -Location $current -Name $part -NoRecursion
        }
        return $current
    }
    
    function ProcessTemplates($cfg) {
        foreach ($clname in $cfg.clusters.Keys) {
    
            $clcfg = $cfg.clusters[$clname]
    
            $folder = Get-FolderByPath -datacenter $cfg.vsphere.datacenter -type VM -path $cfg.vsphere.folder
            $clfolder = Get-FolderByPath -datacenter $cfg.vsphere.datacenter -type VM -path "$($cfg.vsphere.folder)/${clname}" -create:$true
    
            $rpool = Get-ResourcePoolByPath -hostsystem $cfg.vsphere.hostsystem -path $cfg.vsphere.folder
    
            foreach ($poolName in $clcfg.nodePools.Keys) {
    
                $vmname = "${clname}-${poolName}"
    
                Write-Host "[$vmname] Configuring template"
                $vmcfg = $clcfg.nodePools[$poolName]
    
                $template = Get-Template -Name $clcfg.template -Location $folder
    
                $vm = Get-VM -Name $vmname -ErrorAction SilentlyContinue
                if ($null -eq $vm) {
                    Write-Host "[$vmname] Converting existing VM template to a VM for editing..."
                    $vm = Get-Template -Name $vmname -ErrorAction SilentlyContinue | Set-Template -ToVM
                }
                if ($null -eq $vm) {
                    Write-Host "[$vmname] Cloning from $($template.Name)"
                    $vm = New-VM -Name $vmname -Template $template -Datastore $cfg.vsphere.datastore -VMHost $cfg.vsphere.hostsystem -Location $clfolder -ResourcePool $rpool
                }
    
                $vmView = Get-View $vm
                $vmExtensionData = $vm | Get-View
                $vAppConfig = $vmExtensionData.Config.vAppConfig
    
                # vApp config
                # enable static IP pool
                $vAppConfigSpec = New-Object VMware.Vim.VmConfigSpec
                $ipAlloc = New-Object VMware.Vim.VAppIPAssignmentInfo
                $ipAlloc.ipAllocationPolicy = [VMware.Vim.VAppIPAssignmentInfoIpAllocationPolicy]::fixedAllocatedPolicy
                $ipAlloc.supportedAllocationScheme = "ovfenv"
                $ipAlloc.ipProtocol = "IPv4"
                $vAppConfigSpec.ipAssignment = $ipAlloc
                $vAppConfigSpec.OvfEnvironmentTransport = @("com.vmware.guestInfo")
                $vmConfigSpec = New-Object VMware.Vim.VirtualMachineConfigSpec
                $vmConfigSpec.vAppConfig = $vAppConfigSpec
                $_ = $vm.ExtensionData.ReconfigVM_Task($vmConfigSpec)
    
                # processing disks
                # normalize input, $disks must be an array
                $disks = Get-HardDisk -VM $vm
                if ($null -eq $disks) { $disks = @() }
                if ($disks -isnot [array]) { $disks = ,$disks }
    
                # add/resize disks
                Write-Host "[$vmname] Configuring $($vmcfg.disks.Count) disks"
                for ($i=0; $i -lt $vmcfg.disks.Count; $i++) {
                    $size = $vmcfg.disks[$i]
                    if ($i -gt $disks.Length - 1) {
                        # no such disk, create new
                        Write-Host "[$vmname] Adding ${size} GB disk"
                        $disks += New-HardDisk -VM $vm -CapacityGB $size -StorageFormat Thin
                    } else {
                        # existing disk, resize if needed
                        $disk = $disks[$i]
                        if ($disk.CapacityGB -lt $size) {                        
                            Write-Host "[$vmname] Resizing disk $($disk.Name), size ${size}"
                            $_ = Set-HardDisk -HardDisk $disk -CapacityGB $size -Confirm:$false
                        } else {
                            # just report findings
                            Write-Host "[$vmname] Found disk $($disk.Name), size $($disk.CapacityGB) GB (requested ${size})"
                        }
                    }
                }
                #Get-HardDisk -VM $vm | Select-Object Name,CapacityGB
    
                Write-Host "[$vmname] Converting to VM template"
                $vm = Set-VM -VM $vm -ToTemplate -Confirm:$false
            }
        }
    }
    
    function Main() {
        $basedir = $PSScriptRoot
    
        $ErrorActionPreference = 'Stop'
    
        # load yaml configuration
        $cfg = Get-Content -Path "$basedir/vmdeploy.local.yaml" -Raw | ConvertFrom-Yaml
    
        # connect to vCenter
        $con = Connect-VIServer -Server $cfg.vcenter.host -Port $cfg.vcenter.port -Protocol https -User $cfg.vcenter.user -Password $cfg.vcenter.pass -Force
        Write-Host "Connected to $con"
    
        ProcessTemplates($cfg)
    
        Disconnect-VIServer -server $cfg.vcenter.host -Confirm:$false
        Write-Host "Done"
    }
    
    Main
    
---
apiVersion: v1
kind: Secret
metadata:
  name: release-name-vsphere-configure  
  annotations:
    helm.sh/hook: pre-install,pre-upgrade
    helm.sh/hook-delete-policy: before-hook-creation
    helm.sh/hook-weight: "-1"
data:
  vsphere.yaml: CnZjZW50ZXI6CiAgaG9zdDogPG5vIHZhbHVlPgogIHBvcnQ6IDQ0MwogIHVzZXI6IAogIHBhc3M6IAoKdnNwaGVyZTogJnZzcGhlcmUKICBkYXRhY2VudGVyOiA8bm8gdmFsdWU+CiAgaG9zdHN5c3RlbTogPG5vIHZhbHVlPgogIGZvbGRlcjogTEFOL0s4UwogIHBvb2w6IExBTi9LOFMKICBuZXR3b3JrOiA8bm8gdmFsdWU+CiAgc3dpdGNoOiA8bm8gdmFsdWU+CgpjbHVzdGVyczoKICByZWxlYXNlLW5hbWU6CiAgICB2c3BoZXJlOiAqdnNwaGVyZQogICAgc3VibmV0OiA8bm8gdmFsdWU+CiAgICBuZXRtYXNrOiAyNTUuMjU1LjI1NS4wCiAgICBnYXRld2F5OiA8bm8gdmFsdWU+CiAgICBkbnM6IDxubyB2YWx1ZT4KICAgIHBvb2xzOgo=
  vmdeploy.local.yaml: Y2x1c3RlcnM6CiAgJ3JlbGVhc2UtbmFtZSc6CiAgICBub2RlUG9vbHM6CiAgICAgIG1hc3RlcjoKICAgICAgICBkaXNrczoKICAgICAgICAtIDEwCiAgICAgICAgLSAyMAogICAgdGVtcGxhdGU6IGZvby10ZW1wbGF0ZQp2Y2VudGVyOgogIGhvc3Q6IG51bGwKICBwYXNzOiBudWxsCiAgcG9ydDogbnVsbAogIHVzZXI6IG51bGwKdnNwaGVyZToKICBkYXRhY2VudGVyOiBudWxsCiAgZGF0YXN0b3JlOiBudWxsCiAgZm9sZGVyOiBMQU4vSzhTCiAgaG9zdHN5c3RlbTogbnVsbAogIHBvb2w6IExBTi9LOFMK
