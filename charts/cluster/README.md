# cluster

![Version: 0.0.0](https://img.shields.io/badge/Version-0.0.0-informational?style=flat-square)

# Overview

This chart provisions a Rancher managed RKE2 cluster running in vSphere.

```mermaid
flowchart TD
  subgraph 1[VM templates]
  end
  subgraph 2[vSphere Setup]
  end
  subgraph 3[Cluster]

  end
  1-->2-->3
```

```mermaid
sequenceDiagram
  participant helm
  participant job as helm job
  participant rancher
  participant vsphere
  activate helm
  helm->>job: create node pool VM template
  activate job
  job->>vsphere: clone master template to node pool VM template
  job->>vsphere: add disks
  job->>vsphere: resize disks
  deactivate job
  helm->>rancher: deploy cluster CRs
  rancher->>vsphere: create node VM
  activate rancher
  rancher->>vsphere: clone from node pool template
  rancher->>vsphere: assign network
  rancher->>vsphere: assign network

  deactivate rancher
  deactivate helm
```

It creates:
- `Cluster.provisioning.cattle.io/v1` manifest
  - configures RKE2 options
  - defines node pools, their roles, number of VMs, and vSphere template
- `VmwarevsphereConfig.rke-machine-config.cattle.io/v1` manifest, one for each node pool
  - specifies VM options
    - template to clone from
    - number of CPUs, memory, disk size
  - specifies vSphere options
    - datacenter
    - datastore
    - host system
    - folder
    - resource pool
    - network
    - OVF configuration properties, required for IP stack setup
  - cloud config script
- vSphere configuration post-install job
  - prepares vSphere for VM deployment
  - creates folders, network, resource pools, network protocol profiles, IP pools, ...
  - creates dedicated VM template for each node pool
    - clones VM from generic master template
    - sets up disk count and capacity as configured for a given node pool

# networking

Each `cluster` has its own dedicated `subnet`, e.g. `x.x.x.0`.

Each `node pool` has a dedicated IP range within the `cluster subnet`, e.g. `x.x.x.{11..19}`.

`Hostname` is computed from `cluster`, `pool`, and `IP address`, and follows pattern:
- `${clusterName}${poolNumber}${poolName}${vmIndex}`

IP configuration is implemented as follows
- VM is associated with a `network` dedicated to a node pool
- network is associated with a `network protocol profile` dedicated to a given node pool
- network protocol profile
  - defines `IP Pool` that provides IP addresses from an IP range dedicated to a specific node pool
  - defines other IP configuration properties like gateway, netmask, DNS.

# Node Pool VM Templates

Custom resources for VM provisioning support only a single disk with configurable size.
This is obviously insufficient for many deployment use cases and scenarios.

Solution here is the custom provisioning of node pool dedicated VM templates.

With `configure.vmdeploy.enabled=true`, a job is deployed that provisions VM template for each configured
node pool.

Actual RKE2 provisioner is then configured to use this node pool specific VM template.

```mermaid
flowchart LR
  x[jammy-server-cloudimg-amd64.ova]
  x -->|clone & add disks| t1[mycluster-pool1]
  x -->|clone & add disks| t2[mycluster-pool2]
  x -->|clone & add disks| t3[mycluster-pool3]
```

# Configuration

Configure access to your vCenter

```yaml
vcenter:
  host: vcenter.local
  port: 443
  user: user@vcenter.local
  pass: topsecret
```

Specify vSphere objects & layout

```yaml
vsphere:
  datacenter: Datacenter01
  datastore: Datastore01
  host: esxi01
  root: LAN/K8S
  folder: '{{.root}}/{{.cluster}}'
  pool: '{{.root}}/{{.cluster}}'
  network: LAN
  switch: vSwitch0
```

Configure VM templates for your node pools:

```yaml
vcenter: &vcenter { ... } # see above
vsphere: &vsphere { ... } # see above
configure:
  enabled: true
  vmdeploy:
    enabled: true
    config:
      vcenter:
        <<: *vcenter
      vsphere:
        <<: *vsphere
        hostsystem: host01.example.com
        folder: LAN/K8S
        pool: LAN/K8S
      clusters:
        mycluster: # usually defined as {{.Release.Name}}
          template: jammy-server-cloudimg-amd64 # src/base template
          nodePools:
            # configure mycluster-master VM template with single 50GB disk
            master:
              disks: [ 50 ]
            # configure mycluster-worker VM template with two disks (50GB,100GB)
            worker:
              disks: [ 50, 100 ]
```

Declare your cluster & node pools

```yaml
cluster:
  subnet: 10.1.9.0
  gateway: 10.1.9.1
  dns: 10.1.1.1
  pools:
    etcd:
      roles: [ etcd ]
      count: 3
      cpu: 4
      memory: 8000
      disk: 40000
      ipRange: 11..19
    ctrl:
      roles: [ control-plane ]
      count: 2
      cpu: 4
      memory: 8000
      disk: 40000
      ipRange: 21..29
    work:
      roles: [ worker ]
      count: 5
      cpu: 8
      memory: 16000
      disk: 100000
      ipRange: 31..39
templates:
  vmwarevsphereconfig:
    spec:
      cloneFrom: /Datacenter01/vm/LAN/K8S/jammy-server-cloudimg-amd64
---
# or, refer to your dedicated node pool VM template like this
configure:
  enabled: true
  vmdeploy:
    enabled: true
    config: { ... }
templates:
  vmwarevsphereconfig:
    spec:
      cloneFrom: /{{.datacenter}}/vm/{{.folder}}/{{.cluster}}/{{.cluster}}-{{.pool}}
```

Configure registry mirrors (optional):

```yaml
templates:
  cluster:
    spec:
      rkeConfig:
        registries:
          mirrors:
            docker.io:
              endpoints: [ https://harbor.example.com/v2 ]
              rewrite: { "(.*)": "docker-io-project/$1" }

secrets:
  enabled: true
  registries:
    configs:
      harbor.example.com:
        username: reader
        password: reader-password-or-token
```

^This example rewrites all `docker.io/*` image names to `harbor.example.com/docker-io-project/*`,
and image pull is performed using credentials provided in `configs."harbor.example.com"{}` section.

For each `secrets.registries.configs.*` entry:
- a corresponding secret is created
- and registered in `rkeConfig.configs.*.authConfigSecretName`

Configure S3 backup (optional):

```yaml
# configure S3 provisioning job
configure:
  s3backup:
    enabled: true
    url: &s3url https://s3.example.com
    # you need admin credentials here for provisioning user/group/policy
    accessKey: admin
    secretKey: admin123
    ## following user credentials here, these will be used by runtime
    user:
      #name: '{{.cluster}}'
      name: &s3user mycluster
      pass: &s3pass cluster-user-password-123
    buckets:
      region: &s3region eu-west-1

# provision s3 credentials, or skip this to use existing (should match .configure.s3backup)
cloudCredentials:
  items:
    s3backup:
      type: s3
      data:
        s3credentialConfig-defaultEndpoint: *s3url
        s3credentialConfig-accessKey: *s3user
        s3credentialConfig-secretKey: *s3pass
        s3credentialConfig-defaultRegion: *s3region

# enable etcd backup in template, and configure S3 target
templates:
  cluster:
    spec:
      rkeConfig:
        etcd:
          disableSnapshots: false
          #snapshotScheduleCron: "0 7,20 * * *"
          #snapshotRetention: 14
          s3:
            bucket: '{{.cluster}}-etcd'
            ## lookup for s3 credentials provided in .cloudCredentials (default)
            #cloudCredentialName: '{{include "cluster.cloudCredentials.s3" $}}'
            ## or refer to existing credentials explicitly
            cloudCredentialName: cattle-global-data:mycluster-backup

```

Configure VM user(s):

```yaml
cloudConfig:
  users:
  - name: ubuntu
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
    - ssh-rsa ... user@example.com
```

## coredns extensions

This chart extends `rke2-coredns` deployment to include support for `coredns-custom` config map.

Typical use of this extension is as follows:

```yaml
# coredns-custom.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: coredns-custom
  namespace: kube-system
data:
  example.com.conf: |
    example.com:53 {
      file /etc/coredns/custom/example.com.db
    }
  example.com.db: |
    example.com. 60 IN SOA ns.example.com. example.com. (1 60 60 60 60)
    *.example.com. 60 IN CNAME example.com.
    example.com. 60 IN CNAME rke2-ingress-nginx.kube-system.svc.cluster.local.
```

Example above add support for `*.example.com` domain to in-cluster DNS, and redirects whole domain to ingress controller.
Which means an Ingress can be published on `foo.example.com`, and all requests for this host would be resolved to point
to cluster ingress controller.

Following example demonstrates how to intercept only specific DNS queries and redirect them all to ingress
controller.

```yaml
...
apiVersion: v1
kind: ConfigMap
metadata:
  name: coredns-custom
  namespace: kube-system
data:
  aliases.conf: |
    example.com:53 {
      template ANY A {
        match foo.example.com
        match bar.example.com
        answer "cluster 30 IN CNAME rke2-ingress-nginx.kube-system.svc.cluster.local."
      }
      forward . /etc/resolv.conf
      log
      errors
      reload
    }
...
```

---

For additional options, see `values.yaml`

# cloud config

Crucial part of this `#cloud-config` is a `runcmd[]` which runs `scripts/runcmd.sh` script.

It is optimized for `Ubuntu 22.04 LTS`, and does following:
- retrieve OVF environment, and extract following information
  - IP address
  - gateway IP
  - network mask
  - DNS server
- compute hostname as follows
  - initial hostname equals to VM name, and follows the format
    - `${cluster}-${pool}-${uuid}`
  - use provided IP address to provide new host name in format
    - `${cluster}${poolNumber}${poolName}${vmIndex}`
- apply hostname and network changes
- apply some optimizations
  - disable floppy (faster boot, less boot errors)
  - disable `multipathd`
- install k8s tools
  - kubectl
  - k9s
  - set `$KUBECONFIG` to path where RKE2 generates its config

## Node Pool Specific Customization

Node pool template can be customized as follows:

```yaml
templates:
  vmwarevsphereconfig:
    spec: {} # these are defaults
cluster:
  pools:
    pool1:
      templateSpec: {} # here come any overrides
    pool2:
      templateSpec: {} # here come any overrides
```

Similar approach is used to customize `cloudConfig`:

```yaml
cloudConfig: {} # these are defaults

cluster:
  pools:
    pool1:
      cloudConfig: {} # here come any overrides
    pool2:
      cloudConfig: {} # here come any overrides
```

## overriding runcmd script

Default script is in `scripts/runcmd.sh`, and is fed to helm templates by template function `cluster.runcmd`

When extending this chart, user can provide own version of the script, and override the template function so that it
serves customized version of the script.

Special parameter `poolName` is passed to this function which allows differentiating between different pools
and provisioning different script to each and/or selected node pools.

```
#_helpers.tpl
{{- define "cluster.runcmd"}}
{{- $poolName := .poolName}}
{{- .Files.Get "files/custom-runcmd.sh"}}
{{- end}}
```

## overriding bootcmd script

Logic here is similar as described for `runcmd` script above. Template function name is `cluster.bootcmd`.

---

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| vcenter | object | {...} | vcenter connection |
| vcenter.host | string | `nil` | vcenter host |
| vcenter.port | int | `443` | vcenter port |
| vcenter.user | string | `nil` | vcenter username |
| vsphere | object | {...} | vsphere objects |
| vsphere.datacenter | string | `nil` | vsphere datacenter |
| vsphere.datastore | string | `nil` | vsphere datastore |
| vsphere.host | string | `nil` | vsphere hostsystem |
| vsphere.root | string | `nil` | root folder |
| vsphere.folder | string | `"{{.root}}/{{.cluster}}"` | vsphere folder where to store generated objects |
| vsphere.pool | string | `"{{.root}}/{{.cluster}}"` | vsphere resource pool |
| vsphere.network | string | `nil` | name of vsphere network to connect to |
| vsphere.switch | string | `nil` | name of vsphere virtual switch |
| cluster | object | {...} | cluster configuration |
| cluster.subnet | string | `nil` | subnet in format x.x.x.0 |
| cluster.netmask | string | `"255.255.255.0"` | network mask, typically 255.255.255.0 |
| cluster.gateway | string | `nil` | network gateway, typically bsaed on your subnet: x.x.x.1 |
| cluster.dns | string | `nil` | DNS server |
| cluster.network | string | `"{{.cluster}}-{{.pool}}"` | template for network + network profile |
| cluster.pools | object | {} | cluster node pools configuration |
| cluster.pools.$name | object | {} | name of the pool |
| cluster.pools.$name.roles | list | `[]` | roles for this node pool<br>    @values etcd, control-plane, worker |
| cluster.pools.$name.count | int | `1` | number of VMs in this pool |
| cluster.pools.$name.cpu | int | `3` | vCPU count |
| cluster.pools.$name.memory | int | `4000` | RAM in bytes |
| cluster.pools.$name.disk | int | `30000` | disk size in MBs |
| cluster.pools.$name.labels | object | `{"app/$name":"true"}` | node labels |
| cluster.pools.$name.taints | list | `[{"effect":"NoSchedule","key":"app/$name","operator":"Exists"}]` | node taints |
| cluster.pools.$name.templateSpec | object | `{}` | pool-specific overrides of .templates.vmwarevsphereconfig.spec |
| cluster.pools.$name.cloudConfig | object | `{}` | pool-specific overrides of .cloudConfig |
| configure | object | {...} | configure vsphere folders, networks, IP pools |
| configure.enabled | bool | `false` | enabled vsphere configuration job |
| configure.image | string | `"registry.gitlab.com/jrevolt/cicd/charts/cluster/powercli"` | customized powercli image |
| configure.annotations | object | {...} | annotations |
| configure.annotations."helm.sh/hook" | string | `"pre-install,pre-upgrade"` | configure hooks are post-install by default<br>    consider switching to pre-install if cluster provisioning jobs fail because they run sooner    than vsphere provisioning complete<br>    consider adding post-upgrade if you need to apply vsphere configuration changes made after initial install<br> |
| configure.annotations."helm.sh/hook-delete-policy" | string | `"before-hook-creation"` | helm hook delete policy |
| configure.vmdeploy | object | {} | deploys VM templates for node pools, with pre-configured disks (count+sizes) |
| configure.vmdeploy.enabled | bool | `false` | enables VM templates |
| configure.vmdeploy.config | object | {} | configuration of vmdeploy job |
| configure.vmdeploy.config.vcenter | object | {} | vcenter access |
| configure.vmdeploy.config.vsphere | object | {} | vsphere targets |
| configure.vmdeploy.config.clusters | object | {} | configuration of clusters and node pools for vmdeploy tool |
| configure.vmdeploy.config.clusters.$name | object | {} | this is ignored sample only, provide your own name & content |
| configure.vmdeploy.config.clusters.$name.template | string | `nil` | name of the VM template to use as source |
| configure.vmdeploy.config.clusters.$name.nodePools | object | {} | map of node pools |
| configure.vmdeploy.config.clusters.$name.nodePools.$name | object | {} | ignored sample only, provide your own name & content |
| configure.vmdeploy.config.clusters.$name.nodePools.$name.disks | list | [] | array of disk sizes in GB; these will be added and/or resized |
| cloudCredentials | object | {...} | deploy cloud credentials    referenced by `Cluster.spec.cloudCredentialSecretName`, `Cluster.spec.rkeConfig.etcd.s3.cloudCredentialName` |
| cloudCredentials.enabled | bool | `false` | enables credentials deployment |
| cloudCredentials.namespace | string | `"cattle-global-data"` | namespace to deploy cloud credentials to    rancher seems to require specific namespace (cattle-global-data), this does not seem to work in other namespaces |
| cloudCredentials.creatorId | string | `nil` | this maps to `field.cattle.io/creatorId` annotation, Rancher seems to use it for access control    references `management.cattle.io/v3/users` in target cluster    This references user that will be authorized to use these credentials |
| cloudCredentials.items | object | {...} | map of cloud credentials |
| cloudCredentials.items.$name | object | {...} | name of the credential |
| cloudCredentials.items.$name.type | string | `nil` | type of credential, maps to `provisioning.cattle.io/driver` annotation    options: vmwarevsphere|s3 |
| cloudCredentials.items.$name.annotations | object | {...} | credential annotations, both .$name and .creatorId are also mapped here |
| cloudCredentials.items.$name.annotations."provisioning.cattle.io/driver" | string | `nil` | type of credential, set automatically from `.type` |
| cloudCredentials.items.$name.annotations."field.cattle.io/name" | string | `nil` | name of credential, set automatically from `.$name` |
| cloudCredentials.items.$name.annotations."field.cattle.io/creatorId" | string | `nil` | author/owner of the credential, set automatically from `.creatorId` |
| cloudCredentials.items.$name.annotations."helm.sh/resource-policy" | string | `"keep"` | helm resource policy for this credential    must be set to `keep` for `vmwarevsphere` credentials, otherwise cluster uninstall fails because of missing    cloud credentials |
| cloudCredentials.items.$name.data.vmwarevspherecredentialConfig-vcenter | string | `nil` | vcenter host (driver=vmwarevsphere) |
| cloudCredentials.items.$name.data.vmwarevspherecredentialConfig-vcenterPort | string | `nil` | vcenter port (driver=vmwarevsphere) |
| cloudCredentials.items.$name.data.vmwarevspherecredentialConfig-username | string | `nil` | vcenter user name (driver=vmwarevsphere) |
| cloudCredentials.items.$name.data.vmwarevspherecredentialConfig-password | string | `nil` | vcenter password (driver=vmwarevsphere) |
| cloudCredentials.items.$name.data.s3credentialConfig-accessKey | string | `nil` | s3 access key (driver=s3) |
| cloudCredentials.items.$name.data.s3credentialConfig-secretKey | string | `nil` | s3 secret key (driver=s3) |
| cloudCredentials.items.$name.data.s3credentialConfig-defaultEndpoint | string | `nil` | s3 endpoint (driver=s3), e.g. https://s3.example.com |
| cloudCredentials.items.$name.data.s3credentialConfig-defaultRegion | string | `nil` | s3 default region (driver=s3), e.g. eu-west-1 |
| secrets.enabled | bool | `true` |  |
| secrets.registries.configs.$name.username | string | `nil` |  |
| secrets.registries.configs.$name.password | string | `nil` |  |
| templates | object | {...} | templates for spec{} of selected resources |
| templates.cluster | object | {...} | template for cluster.provisioning.cattle.io/v1 CR |
| templates.cluster.enabled | bool | `true` | enables rendering if the CR; mostly for testing |
| templates.cluster.spec | object | {...} | cluster spec{} |
| templates.cluster.spec.cloudCredentialSecretName | string | <computed> | namespace:secret reference to cloud credentials<br>    if .credentials.enabled, this property is automatically set to point to generated credentials;    otherwise this must be reference to existing secret    NOTE: These credentials are expected to be in specific namespace, and this cannot refer to any namespace.          Rancher expects `cattle-global-data` |
| templates.cluster.spec.rkeConfig.registries.mirrors | object | {...} | registry mirrors<br> @see https://docs.rke2.io/install/containerd_registry_configuration?_highlight=mirrors#mirrors |
| templates.cluster.spec.rkeConfig.registries.configs | object | {...} | registry configuration, auth, tls, etc |
| templates.cluster.spec.rkeConfig.chartValues | object | `{"rke2-coredns":{"extraConfig":{"import":{"parameters":"/etc/coredns/custom/*.server"}},"extraVolumeMounts":[{"mountPath":"/etc/coredns/custom","name":"custom-cfg","readonly":true}],"extraVolumes":[{"configMap":{"defaultMode":420,"name":"coredns-custom","optional":true},"name":"custom-cfg"}],"fullnameOverride":"rke2-coredns"}}` | helm chart values for charts deployed as part of the cluster |
| templates.cluster.spec.rkeConfig.chartValues.rke2-coredns | object | {...} | coredns chart, here extended to support coredns-custom config map    coredns-custom is typically deployed as part of haproxy deployment to handle/support access to public    endpoints of services published by this cluster, without the need of external DNS/LB |
| templates.vmwarevsphereconfig | object | {...} | template for vmwarevsphereconfig.rke-machine-config.cattle.io/v1 CR |
| templates.vmwarevsphereconfig.spec | object | {...} | template CR spec caller provides following extra properties: - datacenter - datastore - host - folder - network |
| templates.vmwarevsphereconfig.spec.vappProperty | list | [...] | OVF environment properties<br> Essential for IP stack configuration<br> Values provided by associated vSphere network protocol profile and its IP Pool |
| templates.vmwarevsphereconfig.spec.cloneFrom | string | `"/{{.datacenter}}/vm/{{.folder}}/{{.cluster}}-{{.pool}}"` | VM template to clone from    templating supported, special params are passed in: `.datacenter`, `.folder`, `.cluster`, `.pool` |
| templates.vmwarevsphereconfig.spec.cfgparam | list | `["disk.enableUUID=TRUE"]` | VM configuration parameters    `disk.enableUUID=TRUE` is needed by vSphere CSI |
| templates.vmwarevsphereconfig.spec.cloudConfig | string | {...} | cloud-config, injected from `.Values.cloudConfig` |
| cloudConfig | object | {...} | VmwarevsphereConfig.spec.cloudConfig<br> @see https://cloudinit.readthedocs.io/en/latest/reference/examples.html |
| cloudConfig.chpasswd.expire | bool | `false` | needed to avoid request to change password on first login |
| bootcmd | object | {...} | bootcmd script |
| bootcmd.path | string | `"/etc/cloud/bootcmd.sh"` | path to bootcmd.sh script;    this value is used in write_files[] to create actual script, and in bootcmd[] to invoke the script |
| bootcmd.command | string | calls bootcmd.sh | actual cloudconfig.bootcmd[] command; delegates to a bootmcd.sh script defined above |
| runcmd | object | {...} | runcmd script |
| runcmd.path | string | `"/etc/cloud/runcmd.sh"` | deployment path for the runcmd script |
| runcmd.command | string | calls runcmd.sh, then bootcmd.sh | actual cloudconfig.runcmd[] command; delegates to a runcmd.sh script defined above |
