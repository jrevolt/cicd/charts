{{- define "configuration.categorize"}}
{{- $data := .Values.data}}
{{- $data = omit $data ""}}{{/* prevents IDE confusion when evaluated inline*/}}
{{- $configs_envs := dict}}
{{- $configs_files := dict}}
{{- $secrets_envs:= dict}}
{{- $secrets_files := dict}}
{{- range $k,$v := $data}}
{{- if not (kindIs "map" $v)}}
{{- $v = dict "content" ($v | default "" | toString)}}
{{- end}}
{{- if and $k $v.content}}
{{- $dst := dict}}
{{- if and $v.file $v.secret}}
{{- $dst = $secrets_files}}
{{- else if $v.file}}
{{- $dst = $configs_files}}
{{- else if $v.secret}}
{{- $dst = $secrets_envs}}
{{- else}}
{{- $dst = $configs_envs}}
{{- end}}
{{- $dst = set $dst $k $v}}
{{- end}}
{{- end}}
{{- dict "configs_envs" $configs_envs "configs_files" $configs_files "secrets_envs" $secrets_envs "secrets_files" $secrets_files | toYaml}}
{{- end}}



{{- define "configuration.configs.env"}}
{{- end}}

{{- define "configuration.configs.files"}}
{{- end}}

{{- define "configuration.secrets.env"}}
{{- end}}

{{- define "configuration.secrets.files"}}
{{- end}}

{{- define "configuration.checksum.configs"}}
{{- print $.Template.BasePath "/configs.yaml"}}
{{- end}}

{{- define "configuration.checksum.secrets"}}
{{- print $.Template.BasePath "/secrets.yaml" | sha256sum}}
{{- end}}

{{/*
best effort to render human readable strings
zero effort is
  {{$k}}: {{$v | quote}}
but this produces unreadable entries in config maps data,
especially if they are big multiline strings
hence, we do this, basically, to keep formatting:
  {{- $k}}: |
    {{- $v | nindent 2}}
however, this may add or drop (|-) trailing EOLs, which may be unexpected/undesired

basically, since input is already normalized,
all we need to do is carefully choose between |- and |+
so that input rendered to config map in its original form
*/}}
{{- define "configuration.entry"}}
{{- $k := .key}}
{{- $v := .value}}
{{- $isMultiline := $v | contains "\n"}}
{{- $keep := $v | hasSuffix "\n"}}

{{- if $isMultiline}}
  {{- $ymode := ""}}
  {{- if $keep}}
  {{- $ymode = "|+"}}{{/*keep*/}}
  {{- else}}
  {{- $ymode = "|-"}}{{/*strip*/}}
  {{- end}}
  {{- $k}}: {{$ymode}}{{$v | trimSuffix "\n" | nindent 2}}
{{- else}}
  {{- /* single line */}}
  {{- $k}}: {{$v | quote}}
{{- end}}
{{- end}}


