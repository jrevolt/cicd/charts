
{{- define "utils.helmchart.name"}}
{{- $release := .Release.Name}}
{{- if .Values.fullnameOverride | toString | ne "<nil>"}}
{{- .Values.fullnameOverride}}
{{- else if .Values.nameOverride | toString | ne "<nil>"}}
{{- (printf "%s-%s" $release .Values.nameOverride) | trimSuffix "-"}}
{{- else}}
{{- $name := include "utils.subchart.name" $ | trimPrefix (printf "%s-" .Release.Name)}}
{{- $name | printf "%s-%s" .Release.Name | trunc 63 | trimSuffix "-"}}
{{- end}}
{{- end}}

{{- define "utils.subchart.path"}}
{{- .Template.BasePath | dir | replace "/charts/" "/"}}
{{- end}}

{{- define "utils.subchart.name"}}
{{- include "utils.subchart.path" . | replace "/" "-"}}
{{- end}}

{{- define "utils.image"}}
{{- printf "%s/%s:%s@%s" (.repo | default "") (.name | default "") (.tag | default "") (.sha | default "") | trimPrefix "/" | trimSuffix "@" | trimSuffix ":"}}
{{- end}}

{{/*
These labels are intended to be used strictly as selectors.
See comments in values.yaml
*/}}
{{- define "utils.labels.selector"}}
{{- tpl (.Values.global.labels.selector | toYaml) .}}
{{- end}}

{{/*
Common labels belong to all objects. These are not k8s selectors, and these should not include annotation-like data.
See values.yaml for details.
*/}}
{{- define "utils.labels.common"}}
{{- tpl (.Values.global.labels.common | toYaml) .}}
{{- end}}

{{- define "utils.labels.all"}}
{{- $all := merge dict .Values.global.labels.common .Values.global.labels.selector}}
{{- tpl ($all | toYaml) .}}
{{- end}}

{{- define "utils.template"}}
{{- include (print $.Template.BasePath .templateName) .}}
{{- end}}

{{/*
renders a template identified by $filename, using provided $data
@args: [ data , filename ]
e.g. {{ list $ "configmap.yaml" | include "utils.render"}}
*/}}
{{- define "utils.render"}}
{{- $ := index . 0}}
{{- $file := index . 1}}
{{- include (printf "%s/%s" $.Template.BasePath $file) $}}
{{- end}}

{{- define "utils.render.sha256sum"}}
{{- include "utils.render" . | sha256sum}}
{{- end}}


{{/*
same as "utils.render", but normalizes content by YAML parsing & rendering back to YAML
@see utils.render
*/}}
{{- define "utils.render.yaml"}}
{{- include "utils.render" . | fromYaml | toYaml}}
{{- end}}

{{/*
computes sha256sum for a rendered and normalized template
@see utils.render
*/}}
{{- define "utils.render.yaml.sha256sum"}}
{{- include "utils.render.yaml" . | sha256sum}}
{{- end}}

{{/*
Verifies that an input is an array, or a string parseable as an array.
Fails if the result is not an array.
Renders input as formatted array.
*/}}
{{- define "utils.render.yaml.array"}}
{{- $result := list}}
{{- if and (kindIs "string" .) (trim . | hasPrefix "-")}}
{{- $result = printf "arr:\n%s" (.) | fromYaml | dig "arr" ""}}
{{- else if and (kindIs "string" .) (trim . | hasPrefix "[")}}
{{- $result = printf "arr: %s" (.) | fromYaml | dig "arr" ""}}
{{- else}}
{{- $result = .}}
{{- end}}
{{- if $result | kindIs "slice" | not}}
{{- printf "This does not look like an array: %s" (toString $result | squote) | fail}}
{{- end}}
{{- $result | toYaml}}
{{- end}}

{{/*
Removes all empty keys/values from input, and renders result.
Typically used like this, to determine if the input is logically empty:
{{- if include "utils.emptify" .Values.foo | fromJson}}
...
{{- end}}
*/}}
{{- define "utils.emptify"}}
{{- $data := . | default dict | deepCopy}}
{{- if . | kindIs "map" | not}}{{$data = dict "data" $data}}{{end}}
{{- include "utils.emptify.map" $data}}
{{- if . | kindIs "map" | not}}{{$data = get $data "data"}}{{end}}
{{- if $data | kindIs "string"}}{{$data = $data | eq "" | ternary nil $data}}{{end}}
{{- $data | toJson}}
{{- end}}

{{/*
Internal function that manipulates input map; no output is rendered.
@see utils.emptify
*/}}
{{- define "utils.emptify.map"}}
{{- $data := .}}
{{- range $k,$v := $data}}
  {{- if $v | kindOf | hasPrefix "map"}}
    {{- include "utils.emptify.map" $v}}
  {{- else if $v | kindIs "slice"}}
    {{- $arg := dict "list" $v}}
    {{- include "utils.emptify.slice" $arg}}
    {{- $v = $arg.list}}
  {{- else}}
    {{- /* noop for scalars */}}
  {{- end}}
  {{- /* save if "truthy", drop/remove if "falsey" */}}
  {{- if $v}}
    {{- $data = set $data $k $v}}
  {{- else}}
    {{- $data = unset $data $k}}
  {{- end}}
{{- end}}
{{- end}}

{{/*
Internal function that manipulates input slice; no output is rendered.
@arg `.list` contains the input array/slice; result is a new slice written back here
@see utils.emptify
*/}}
{{- define "utils.emptify.slice"}}
{{- $data := .list}}
{{- $new := list}}
{{- range $v := $data}}
  {{- if $v | kindIs "map"}}
    {{- include "utils.emptify.map" $v}}
  {{- else if $v | kindIs "slice"}}
    {{- $arg := dict "list" $v}}
    {{- include "utils.emptify.slice" $arg}}
    {{- $v = $arg.list}}
  {{- else}}
    {{- /* noop for scalars */}}
  {{- end}}
  {{- /* save to result only if object is "truthy" */}}
  {{- if $v}}
    {{- $new = append $new $v}}
  {{- end}}
{{- end}}
{{- $_ := set . "list" $new}}
{{- end}}
