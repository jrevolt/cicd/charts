# INSTALL

## Install helm chart

Prerequisites: # TODO

Command:

helm -n <namespace> upgrade -i <releaseName> . -f <values.$ENV.yaml>

## Upgrade helm chart

helm -n <namespace> upgrade -i <releaseName> . -f <values.$ENV.yaml>

## Uninstall helm chart

helm -n <namespace> uninstall <releaseName>