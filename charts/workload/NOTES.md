# notes

## command/args templating

Deployment's `command[]` and `args[]` can also be provided as strings which is useful in conjunction
with templating.

Following example demonstrates different configuration producing the same result:

```yaml
---
deployment:
  command: [ sh, -c ]
  args: [ "echo hey" ]
---
deployment:
  enableTemplatedCommandAndArgs: true
  command: [ sh, -c ]
  # provided as array, whole array is rendered `toYaml` and used as template
  args:
  - |
    {{- .Values.deployment.script | nindent 2}}
  script: |
    echo hey
---
deployment:
  enableTemplatedCommandAndArgs: true
  command: [ sh, -c ]
  # provided as single string, used as template that produces YAML array
  args: |
    - |
      {{- .Values.deployment.script | nindent 2}}
  script: |
    echo hey
```

Also, when templating command/args, consider following examples/gotchas:

```yaml
---
deployment:
  enableTemplatedCommandAndArgs: true
  command: [ sh, -c ]
  args:
  # OK: this is the working standard, use this by default
  - |
    {{.Values.deployment.script | nindent 2}}
  # NOK: this will eat your EOLs
  - '{{.Values.deployment.script}}'
  # OK/risky: this will work for single line script with no special YAML-breaking chars
  - '{{.Values.deployment.script1}}'
  # NOK: apostrophes will break resulting yaml
  - '{{.Values.deployment.script2}}'
  # NOK: leading bracket will break resulting yaml
  - '{{.Values.deployment.script3}}'
  script: |
    echo hey
    echo bye
  script1: echo hey; echo bye
  script2: echo 'hey'; echo 'bye'
  script3: '{ echo hey; echo bye; }'
```
