# workload

## Chart version: 0.1.0

## deployment

### command/args

In addition to standard plain `command[]` and `args[]` arrays,
templating can be enabled using `enableTemplatedCommandAndArgs=true`,
and whole arrays can be provided also as single string template.
Output of such string template must be a valid parseable YAML array.

This is useful when a single template produces variable length array.

For details, see `tests/values.cmdargs*.yaml` and corresponding `test/results/*`.

For additional notes/gotchas, see [NOTES.md](./NOTES.md)

## ports

Chart supports declaration of named pod ports and their use on expected places like
`Service`, `PodMonitor`, `ServiceMonitor`.

By example:

### publish port (complex)

```yaml
deployment:
  ports:
    http: 8080
```

### publish port (complex)

```yaml
deployment:
  ports:
    dns:
      containerPort: 53
      protocol: UDP
```

### publish on service

```yaml
deployment:
  ports:
    http: 8080
service:
  enabled: true
  ports:
    http:
      port: 80 # optional, default uses containerPort
```

### metrics

PodMonitors:

```yaml
deployment:
  ports:
    http: 8080
    metrics: 9094
metrics:
  enabled: true
  podMonitors:
    metrics: { interval: 60s }
```

ServiceMonitors:

```yaml
deployment:
  ports:
    metrics: 9094
service:
  enabled: true
  ports:
    metrics: {}
metrics:
  enabled: true
  serviceMonitors:
    metrics: { interval: 60s }
```

## volumes

Chart offers composite syntax do define volumes and their usage all in one place

By example:

### simple emptyDir{} volume

```yaml
deployment:
  volumes:
    my-vol:
      mounts:
      - mountPath: /data
```

### with PVC

this will create PVC, references it in volumes[] and then uses such volume in volumeMounts[]

```yaml
deployment:
  volumes:
    my-vol:
      mounts:
      - mountPath: /data
      pvc:
        spec:
          accessModes: [ ReadWriteOnce ]
          resources:
            requests:
              storage: 1Gi
```

if you want to keep the data upon helm uninstall, don't forget this:

```yaml
deployment:
  volumes:
    my-vol:
      pvc:
        annotations:
          helm.sh/resource-policy: keep
```

### use existing PVC

```yaml
deployment:
  volumes:
    my-vol:
      mounts:
      - mountPath: /data
      spec:
        persistentVolumeClaim:
          claimName: existing-pvc
```

however, for simple use of existing PVC, use `references`:

```yaml
deployment: {}
references:
  /data1: pvc/name
  /data2: pvc/name/subpath
```

### full spec

```yaml
deployment:
  volumes:
    my-vol:
      # optional; create PV
      pv:
        labels: {}
        annotations: {}
        spec: {}
      # optional; creates PVC
      pvc:
        labels: {}
        annotations: {}
        spec: {}
      # if set, provided pvc{} will be used in stateful set's volumeClaimTemplates[]
      volumeClaimTemplate: false
      # optional, content of volume[] item; if omitted, k8s defaults to emptyDir{}
      # if pvc{} is defined and spec{} is missing, persistentVolumeClaim{} generated
      spec: {}
      # optional; creates volumeMounts associated with this volume
      mounts:
      - mountPath: /data1
        subPath: path/to/folder
references:
  files:
    # mounts existing PVC
    /mnt/path1: pvc/$name
    # mounts given subPath in exiting PVC
    /mnt/path2: pvc/$name/subpath/in/volume

```

# metrics / monitoring

Chart supports optional pod/service monitors.

Example

```yaml
deployment:
  ports:
    podMetrics1: 9904
    svcMetrics1: 9905
service:
  enabled: true
  ports:
    svcMetrics1: {} #io.k8s.api.core.v1.ServicePort[]
metrics:
  enabled: true
  podMonitors:
    podMetrics1: {} #podmonitors.monitoring.coreos.com.v1:spec:podMetricsEndpoints:items[]
  serviceMonitors:
    svcMetrics1: {} #servicemonitors.monitoring.coreos.com.v1:spec:endpoints:items[]

```
---

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| file://./../configuration | configuration | * |
| file://./../ingress | ingress | * |
| file://./../utils | utils | * |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| annotations | object | `{}` |  |
| configuration.enabled | bool | `true` |  |
| configuration.nameOverride | string | `"cfg"` |  |
| deployment.affinity | object | `{}` |  |
| deployment.annotations | object | `{}` |  |
| deployment.args | list | `[]` |  |
| deployment.command | list | `[]` |  |
| deployment.container | object | `{}` | extra options to include in containers[], e.g. tty=true, etc |
| deployment.cronjob.annotations | object | `{}` |  |
| deployment.cronjob.concurrencyPolicy | object | `{}` |  |
| deployment.cronjob.failedJobsHistoryLimit | object | `{}` |  |
| deployment.cronjob.schedule | string | `"0 * * * *"` |  |
| deployment.cronjob.startingDeadlineSeconds | object | `{}` |  |
| deployment.cronjob.successfulJobsHistoryLimit | object | `{}` |  |
| deployment.cronjob.suspend | bool | `true` |  |
| deployment.enableTemplatedCommandAndArgs | bool | `false` |  |
| deployment.enabled | bool | `true` |  |
| deployment.image.name | string | `""` |  |
| deployment.image.repo | string | `""` |  |
| deployment.image.secret | string | `""` | single secret name type:dockerconfigjson |
| deployment.image.sha | string | `""` |  |
| deployment.image.tag | string | `""` |  |
| deployment.imagePullSecrets | list | `[]` |  |
| deployment.initContainers | list | `[]` |  |
| deployment.job.annotations | object | `{}` |  |
| deployment.job.backoffLimit | object | `{}` |  |
| deployment.job.completionMode | object | `{}` |  |
| deployment.job.completions | object | `{}` |  |
| deployment.job.parallelism | object | `{}` |  |
| deployment.job.ttlSecondsAfterFinished | object | `{}` |  |
| deployment.kind | Deployment|StatefulSet|Job|CronJob | `"Deployment"` |  |
| deployment.nodeSelector | object | `{}` |  |
| deployment.pod.activeDeadlineSeconds | object | `{}` |  |
| deployment.pod.annotations | object | `{}` |  |
| deployment.pod.enableServiceLinks | bool | `false` |  |
| deployment.pod.restartPolicy | object | `{}` |  |
| deployment.ports | object | `{}` | exposed ports simplified form name:number is rendered as { name: $name, containerPort: $number } full form name:map{} is rendered as is |
| deployment.probes.enabled | bool | `false` |  |
| deployment.probes.liveness | object | `{}` |  |
| deployment.probes.readiness | object | `{}` |  |
| deployment.probes.startup | object | `{}` |  |
| deployment.resources | object | `{}` |  |
| deployment.securityContext | object | `{}` |  |
| deployment.serviceAccountName | object | `{}` |  |
| deployment.tolerations | list | `[]` |  |
| deployment.topologySpreadConstraints | list | `[]` |  |
| deployment.volumes | object | `{}` |  |
| fullnameOverride | string | `""` |  |
| hpa.annotations | object | `{}` |  |
| hpa.enabled | bool | `false` |  |
| hpa.maxReplicas | object | `{}` |  |
| hpa.minReplicas | object | `{}` |  |
| hpa.targetCPUUtilizationPercentage | object | `{}` |  |
| ingress.enabled | bool | `false` |  |
| metrics.enabled | bool | `false` |  |
| metrics.podMonitor | object | {...} | must have at least 1 endpoint, otherwise ignored |
| metrics.podMonitor.endpoints | object | `{}` | map of port monitors key is a named port from .deployment.ports{}, value is a dict compatible with podmonitors.monitoring.coreos.com.v1:spec:podMetricsEndpoints:items[] |
| metrics.podMonitor.spec | object | `{}` | podmonitors.monitoring.coreos.com.v1:spec except selector{} and podMetricsEndpoints[] |
| metrics.serviceMonitor | object | {...} | must have at least 1 endpoint, otherwise ignored |
| metrics.serviceMonitor.endpoints | object | `{}` | map of service monitors key is a named port from .service.ports[] value is a dict compatible with servicemonitors.monitoring.coreos.com.v1:spec:endpoints:items[] |
| metrics.serviceMonitor.spec | object | `{}` | servicemonitors.monitoring.coreos.com.v1:spec except selector{} and endpoints[] |
| nameOverride | string | `""` |  |
| pdb.annotations | object | `{}` |  |
| pdb.enabled | bool | `false` |  |
| pdb.maxUnavailable | object | `{}` |  |
| pdb.minAvailable | int | `1` |  |
| references | object | `{}` |  |
| service.annotations | object | `{}` |  |
| service.enabled | bool | `false` |  |
| service.ports | object | `{}` | map of service ports key is a port name value is a map compatible with io.k8s.api.core.v1.ServicePort[] legacy: can also be ports[], in which case the list is used and rendered as is |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.12.0](https://github.com/norwoodj/helm-docs/releases/v1.12.0)